package com.blankie.unshortify;

import android.os.Bundle;
import android.app.Activity;
import android.widget.Toast;
import android.content.Intent;
import android.content.ActivityNotFoundException;
import android.net.Uri;
import android.util.Log;
import android.webkit.URLUtil;
import java.util.Locale;

import com.blankie.unshortify.R;

public class ShareActivity extends Activity {
    public static final String TAG = "unshortify";

    private void logVariable(final String name, final String value) {
        String msg = name + " = ";
        if (value != null) {
            msg += "[" + value + "]";
        } else {
            msg += "null";
        }
        Log.d(TAG, msg);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            final Intent intent = getIntent();
            final String urlString = intent.getStringExtra(Intent.EXTRA_TEXT);
            logVariable("urlString", urlString);
            if (urlString == null || urlString.isEmpty()) {
                Toast.makeText(getApplicationContext(), R.string.empty_url,
                    Toast.LENGTH_SHORT).show();
                return;
            }

            final Uri urlParsed = Uri.parse(URLUtil.guessUrl(urlString));
            final String scheme = urlParsed.getScheme() != null
                    ? urlParsed.getScheme().toLowerCase(Locale.ROOT)
                    : null;
            logVariable("scheme", scheme);
            if (scheme == null || (!scheme.equals("http")
                    && !scheme.equals("https"))) {
                Toast.makeText(getApplicationContext(), R.string.unknown_scheme,
                    Toast.LENGTH_SHORT).show();
                return;
            }

            final String host = urlParsed.getHost() != null
                    ? urlParsed.getHost().toLowerCase(Locale.ROOT)
                    : null;
            logVariable("host", host);
            if (host == null || (!host.equals("youtube.com") &&
                    !host.endsWith(".youtube.com") &&
                    !host.equals("youtu.be"))) {
                Toast.makeText(getApplicationContext(), R.string.non_youtube_url,
                    Toast.LENGTH_SHORT).show();
                return;
            }

            final String path = urlParsed.getPath();
            logVariable("path", path);
            if (path == null) {
                Toast.makeText(getApplicationContext(), R.string.null_path,
                    Toast.LENGTH_SHORT).show();
                return;
            }

            String videoId = null;
            if (host.equals("youtu.be")) {
                final String[] pathSegments = urlParsed.getPath().split("/");
                if (pathSegments.length == 2) {
                    videoId = pathSegments[1];
                }
            } else if (path.toLowerCase(Locale.ROOT).startsWith("/shorts/")) {
                final String[] pathSegments = urlParsed.getPath().split("/");
                if (pathSegments.length == 3) {
                    videoId = pathSegments[2];
                }
            }
            if (videoId == null || videoId.isEmpty()) {
                Toast.makeText(getApplicationContext(), R.string.non_shorts_url,
                    Toast.LENGTH_SHORT).show();
                return;
            }

            Uri.Builder builder = new Uri.Builder();
            final Uri unshortifiedUrl = builder.scheme("https")
                .authority("www.youtube.com")
                .path("/watch")
                .appendQueryParameter("v", videoId)
                .build();

            final Intent unshortifiedIntent = new Intent(Intent.ACTION_VIEW, unshortifiedUrl);
            try {
                startActivity(unshortifiedIntent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), R.string.no_activity_found,
                    Toast.LENGTH_SHORT).show();
            }
        } finally {
            finish();
        }
    }
}
